variable "launch_template_id" {
    type = string 
}
variable "launch_template_version" {
    type = string
}
variable "region" {
    type = string
}
variable "cluster_name" {
    type = string
}
variable "image_id" {
    type = string
}
variable "instance_type" {
    type = string
}
variable "key_name" {
    type = string
}

variable "device_index" {
    type = number
}
variable "security_group_id" {
    type = list(string)
}
variable "max_size" {
    type = number
}
variable "min_size" {
    type = number
}
variable "health_check_grace_period" {
    type = number
}
variable "health_check_type" {
    type = string
}
variable "availability_zones" {
    type = list(string)
}
variable "private_ip" {
    type = list(string)
}
variable "subnet_cidr_block" {
    type = string
}
variable "vpc_cidr_block" {
    type = string
}
variable "vpc_name" {
    type = string
}
variable "subnet_id" {
    type = string
}
variable "vpc_id" {
    type = string
}