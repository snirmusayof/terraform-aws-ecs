launch_template_id = "lt-0a49476bca8ba233f"
launch_template_version = "12"
region = "eu-central-1"
cluster_name = "elta"
image_id = "ami-0ca47779698ddd611"
instance_type = "m5.large"
key_name = "slnd"
device_index = 0
security_group_id = ["sg-03a62c9bbf608c6b9"]
max_size = 1
min_size = 0
health_check_grace_period = 0
health_check_type = "EC2"
availability_zones = ["eu-central-1b"]
private_ip = ["10.0.2.5"]
subnet_cidr_block = "10.0.2.0/24"
vpc_cidr_block = "10.0.0.0/16"
vpc_name = "customer-vpc"
subnet_id = "subnet-0b5dc2b4d571cbffd"
vpc_id = "vpc-0eea34da33399f0e7"