# Configure the AWS Provider
locals {
  cluster_name = "${var.cluster_name}"
}
provider "aws" {
  region = var.region
 default_tags {
   tags = {
     MangedBy = "Terraform"
     }
 }
}

# Create VPC
resource "aws_vpc" "this" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name = var.vpc_name
  }
}
# Create Subnet
resource "aws_subnet" "this" {
  vpc_id     = var.vpc_id
  cidr_block = var.subnet_cidr_block
  tags = {
    Name = "${var.cluster_name}-private"
  }
}
# Create Network-interface
resource "aws_network_interface" "this" {
  subnet_id       = var.subnet_id
  private_ips     = var.private_ip
}

# Create Autoscaling-Group
resource "aws_autoscaling_group" "this" {
  name                      = var.cluster_name
  max_size                  = var.max_size
  min_size                  = var.min_size
  health_check_grace_period = var.health_check_grace_period
  health_check_type         = var.health_check_type
  launch_template {
    id = var.launch_template_id
    version = var.launch_template_version
  }
  availability_zones = var.availability_zones
  protect_from_scale_in = true
}

# Create Capacity provider
resource "aws_ecs_capacity_provider" "this" {
  name = "capacity_provider_${var.cluster_name}"
  auto_scaling_group_provider {
    auto_scaling_group_arn         = aws_autoscaling_group.this.arn
    managed_termination_protection = "ENABLED"

    managed_scaling {
      maximum_scaling_step_size = 1000
      minimum_scaling_step_size = 1
      status                    = "ENABLED"
      target_capacity           = 100
    }
  }
}

# Create ECS Cluster
resource "aws_ecs_cluster" "this" {
  name = var.cluster_name
}

# Update Capacity provider
resource "aws_ecs_cluster_capacity_providers" "this" {
  cluster_name = aws_ecs_cluster.this.id
  capacity_providers = [ aws_ecs_capacity_provider.this.name ]
  default_capacity_provider_strategy {
    capacity_provider = aws_ecs_capacity_provider.this.name
    base              = 1
    weight            = 100
  }
}