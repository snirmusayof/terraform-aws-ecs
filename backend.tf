terraform {
  backend "s3" {
    bucket = "soi-terraform"
    region = "eu-central-1"
  }
}
